#!/bin/bash
current_dir=$(pwd)
script_dir=$(dirname $0)
base_dir=$current_dir"/"$script_dir
mkdir -p $base_dir/volume
docker stop toran
docker rm -f toran
docker run -d -p 8005:80 --name toran -v $base_dir/volume:/data/toran-proxy -w /data/toran-proxy cedvan/toran-proxy